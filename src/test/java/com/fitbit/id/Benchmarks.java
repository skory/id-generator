package com.fitbit.id;

import com.fitbit.id.clock.SystemClock;
import com.fitbit.id.exception.InvalidClockException;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Group;
import org.openjdk.jmh.annotations.GroupThreads;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Threads;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;

import java.util.concurrent.TimeUnit;

/**
 * @author Boris Skarabahaty.
 */
@Threads(16)
@Warmup(iterations = 1, time = 1)
@Measurement(iterations = 5, time = 1)
public class Benchmarks {
    @State(Scope.Benchmark)
    public static class BlockedState {
        private final BlockedIdGenerator blocked = new BlockedIdGenerator(new SystemClock(), 1);

        public BlockedState() {
        }
    }

    @State(Scope.Benchmark)
    public static class LockFreeState {
        private final LockFreeIdGenerator lockFree = new LockFreeIdGenerator(new SystemClock(), 1, 100);

        public LockFreeState() {
        }
    }

    public Benchmarks() {
    }

    @Fork(value = 1)
    @Benchmark
    @BenchmarkMode({Mode.All})
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    public void measureLockFree(LockFreeState state, Blackhole blackhole) {
        blackhole.consume(state.lockFree.nextId());
    }

    @Fork(value = 1)
    @Benchmark
    @BenchmarkMode({Mode.All})
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    public void measureBlocked(BlockedState state, Blackhole blackhole) throws InvalidClockException {
        blackhole.consume(state.blocked.nextId());
    }
}
