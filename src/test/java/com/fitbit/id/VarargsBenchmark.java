package com.fitbit.id;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Threads;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;

import java.util.concurrent.TimeUnit;

/**
 * @author Boris Skarabahaty.
 */
@Threads(1)
@Warmup(iterations = 1, time = 1)
@Measurement(iterations = 5, time = 1)
public class VarargsBenchmark {
    @State(Scope.Benchmark)
    public static class BenchmarkState {
        public String call1(String arg) {
            return arg;
        }

        public String[] call2(String... args) {
            return args;
        }
    }

    @Fork(value = 1)
    @Benchmark
    @BenchmarkMode({Mode.All})
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    public void measureSimple(BenchmarkState state, Blackhole blackhole) {
        blackhole.consume(state.call1("1"));
    }

    @Fork(value = 1)
    @Benchmark
    @BenchmarkMode({Mode.All})
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    public void measureVarargs(BenchmarkState state, Blackhole blackhole) {
        blackhole.consume(state.call2("1"));
    }
}
