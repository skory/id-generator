package com.fitbit.id.clock;

/**
 * @author Boris Skarabahaty.
 */
public interface Clock {
    long millis();
}
