package com.fitbit.id.clock;

/**
 * @author Boris Skarabahaty.
 */
public final class SystemClock implements Clock {
    public long millis() {
        return System.currentTimeMillis();
    }
}
