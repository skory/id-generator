package com.fitbit.id;

import com.fitbit.id.clock.Clock;
import com.fitbit.id.clock.SystemClock;
import com.fitbit.id.exception.GetHardwareIdFailedException;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReferenceArray;

/**
 * @author Boris Skarabahaty.
 */
public final class LockFreeIdGenerator implements IdGenerator {
    private final Clock clock;
    private final long machine;
    private final AtomicReferenceArray<Cell> state;

    public LockFreeIdGenerator() throws GetHardwareIdFailedException {
        this(new SystemClock());
    }

    public LockFreeIdGenerator(Clock clock) throws GetHardwareIdFailedException {
        this(clock, Utils.machineId(), 100);
    }

    public LockFreeIdGenerator(long machine) {
        this(new SystemClock(), machine, 100);
    }

    public LockFreeIdGenerator(Clock clock, long machine, int stateSize) {
        this.clock = clock;
        this.machine = machine;
        this.state = new AtomicReferenceArray<>(new Cell[stateSize]);
        for (int i = 0; i < stateSize; ++i) state.set(i, new Cell(0));
    }

    public long nextId() {
        long millis = clock.millis();
        long sequence;
        while (true) {
            Cell cell = findCell(millis);
            sequence = cell.sequence.getAndIncrement();
            if (sequence > Utils.SEQUENCE_MAX) {
                millis = Utils.awaitTillNextMillis(clock, millis);
                continue;
            }

            break;
        }

        return ((millis - Utils.TW_EPOCH) << Utils.MILLIS_SHIFT)
            | (machine << Utils.MACHINE_SHIFT)
            | sequence;
    }

    private Cell findCell(long millis) {
        int index = (int) (millis % state.length());
        Cell update;
        while (true) {
            Cell cell = state.get(index);
            if (cell.millis >= millis) return cell;
            if (state.weakCompareAndSet(index, cell, update = new Cell(millis))) return update;
        }
    }

    private static final class Cell implements Comparable<Cell> {
        private final long millis;
        private final AtomicLong sequence = new AtomicLong(0);

        private Cell(long millis) {
            this.millis = millis;
        }

        @Override
        public int compareTo(Cell other) {
            return Long.compare(millis, other.millis);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Cell)) return false;
            Cell cell = (Cell) o;
            return millis == cell.millis;
        }

        @Override
        public int hashCode() {
            return Objects.hash(millis);
        }
    }
}
