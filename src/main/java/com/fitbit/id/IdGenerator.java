package com.fitbit.id;

import com.fitbit.id.exception.InvalidClockException;

/**
 * @author Boris Skarabahaty.
 * id is composed of:
 * time - 41 bits (millisecond precision w/ a custom epoch gives us 69 years)
 * configured machine id - 10 bits - gives us up to 1024 machines
 * sequence number - 12 bits - rolls over every 4096 per machine (with protection to avoid rollover in the same ms)
 */
public interface IdGenerator {
    //   id format  =>
    //   timestamp |machine | sequence
    //   41        |10         |  12
    long nextId() throws InvalidClockException;
}
