package com.fitbit.id.exception;

/**
 * @author Boris Skarabahaty.
 */
public final class InvalidClockException extends Exception {
    public InvalidClockException(String message) {
        super(message, null, true, false);
    }
}
