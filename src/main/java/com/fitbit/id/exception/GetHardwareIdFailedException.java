package com.fitbit.id.exception;

/**
 * @author Boris Skarabahaty.
 */
public final class GetHardwareIdFailedException extends Exception {
    public GetHardwareIdFailedException(String reason) {
        super(reason, null, true, false);
    }

    public GetHardwareIdFailedException(Exception e) {
        super("", e, true, false);
    }
}
