package com.fitbit.id;

import com.fitbit.id.clock.Clock;
import com.fitbit.id.clock.SystemClock;
import com.fitbit.id.exception.GetHardwareIdFailedException;
import com.fitbit.id.exception.InvalidClockException;

/**
 * @author Boris Skarabahaty.
 */
public final class BlockedIdGenerator implements IdGenerator {
    private final Clock clock;
    private final long machine;

    private long lastMillis;
    private long sequence;

    public BlockedIdGenerator() throws GetHardwareIdFailedException {
        this(new SystemClock());
    }

    public BlockedIdGenerator(Clock clock) throws GetHardwareIdFailedException {
        this(clock, Utils.machineId());
    }

    public BlockedIdGenerator(long machine) {
        this(new SystemClock(), machine);
    }

    public BlockedIdGenerator(Clock clock, long machine) {
        this.clock = clock;
        this.machine = machine;
    }

    @Override
    public synchronized long nextId() throws InvalidClockException {
        long millis = clock.millis();
        if (millis < lastMillis) {
            throw new InvalidClockException("Clock moved backwards.  Refusing to generate id for " + (
                lastMillis - millis) + " milliseconds.");
        }

        if (lastMillis == millis) {
            sequence = (sequence + 1) % Utils.SEQUENCE_MAX;
            if (sequence == 0) millis = Utils.awaitTillNextMillis(clock, millis);
        } else {
            lastMillis = millis;
            sequence = 0;
        }

        return ((millis - Utils.TW_EPOCH) << Utils.MILLIS_SHIFT)
            | (machine << Utils.MACHINE_SHIFT)
            | sequence;
    }
}
