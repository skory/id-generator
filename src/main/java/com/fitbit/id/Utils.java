package com.fitbit.id;

import com.fitbit.id.clock.Clock;
import com.fitbit.id.exception.GetHardwareIdFailedException;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * @author Boris Skarabahaty.
 * id is composed of:
 * time - 41 bits (millisecond precision w/ a custom epoch gives us 69 years)
 * configured machine id - 10 bits - gives us up to 1024 machines
 * sequence number - 12 bits - rolls over every 4096 per machine (with protection to avoid rollover in the same ms)
 */
public abstract class Utils {
    private Utils() {
    }

    public static final long SEQUENCE_BITS = 12;
    public static final long MACHINE_BITS = 10L;

    public static final long MACHINE_SHIFT = SEQUENCE_BITS;
    public static final long MILLIS_SHIFT = SEQUENCE_BITS + MACHINE_BITS;

    public static final long TW_EPOCH = 1288834974657L;
    public static final long SEQUENCE_MAX = 4096;

    public static long machineId() throws GetHardwareIdFailedException {
        try {
            InetAddress ip = InetAddress.getLocalHost();
            NetworkInterface network = NetworkInterface.getByInetAddress(ip);
            if (network == null) {
                return 1;
            } else {
                byte[] mac = network.getHardwareAddress();
                return ((0x000000FF & (long) mac[mac.length - 1])
                    | (0x0000FF00 & (((long) mac[mac.length - 2]) << 8)))
                    >> 6;
            }
        } catch (SocketException | UnknownHostException e) {
            throw new GetHardwareIdFailedException(e);
        }
    }

    public static long awaitTillNextMillis(Clock clock, long last) {
        long millis = clock.millis();
        while (millis <= last) millis = clock.millis();

        return millis;
    }
}
